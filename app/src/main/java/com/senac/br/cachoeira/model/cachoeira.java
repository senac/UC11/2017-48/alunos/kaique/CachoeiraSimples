package com.senac.br.cachoeira.model;

import android.graphics.Bitmap;

public class cachoeira {

    private int id   ;
    private String nome ;
    private String informocoes ;
    private  transient Bitmap imagem  ;
    private float classificacao ;
    private String email ;
    private String telefone ;
    private String endereco ;
    private String  site ;

    public cachoeira () {
    }

    public cachoeira(int id, String nome, String informocoes, Bitmap imagem, float classificacao, String email, String telefone, String endereco, String site) {
        this.id = id;
        this.nome = nome;
        this.informocoes = informocoes;
        this.imagem = imagem;
        this.classificacao = classificacao;
        this.email = email;
        this.telefone = telefone;
        this.endereco = endereco;
        this.site = site;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getInformocoes() {
        return informocoes;
    }

    public void setInformocoes(String informocoes) {
        this.informocoes = informocoes;
    }

    public Bitmap getImagem() {
        return imagem;
    }

    public void setImagem(Bitmap imagem) {
        this.imagem = imagem;
    }

    public float getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(float classificacao) {
        this.classificacao = classificacao;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }
}
