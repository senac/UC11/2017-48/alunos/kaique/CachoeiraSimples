package com.senac.br.cachoeira.View;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.senac.br.cachoeira.R;

public class MainActivity extends AppCompatActivity {

    private Button riodomeio;
    private Button matilde;
    private Button veudenoiva;
    private Button palto;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        riodomeio =(Button) findViewById(R.id.btnC_R_Meio);
        matilde = findViewById(R.id.btnC_Matilde);
        veudenoiva =findViewById(R.id.btnC_V_Noiva);
        palto = findViewById(R.id.btnC_Palito);


        /* Comando do meu botao onde chamo meu outro Layout da outra janela */

        riodomeio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this , RioMeioActivity.class );

                /* String texto = riodomeio.getText().toString() ;

                intent.putExtra("nome" , texto) ; */

                startActivity(intent) ;

            }
        });

        matilde.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this , MatildeActivity.class );

                /* String texto = riodomeio.getText().toString() ;

                intent.putExtra("nome" , texto) ; */

                startActivity(intent) ;
            }
        });

        veudenoiva.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this , VeudeNoivaActivity.class );

                /* String texto = riodomeio.getText().toString() ;

                intent.putExtra("nome" , texto) ; */

                startActivity(intent) ;
            }
        });

        palto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this , PalitoActivity.class );

                /* String texto = riodomeio.getText().toString() ;

                intent.putExtra("nome" , texto) ; */

                startActivity(intent) ;
            }
        });













    }
}
